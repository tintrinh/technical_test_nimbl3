class GoogleResultsController < ApplicationController
  before_action :set_google_result, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /google_results
  # GET /google_results.json
  def index
    @google_results = GoogleResult.by_upload(params[:id])
    @upload_histories = [['Choose a report', -1]] + UploadHistory.all.pluck(:created_at, :id)
  end

  # GET /google_results/1
  # GET /google_results/1.json
  def show
  end

  # GET /google_results/new
  def new
    @google_result = GoogleResult.new
  end

  # GET /google_results/1/edit
  def edit
  end

  # POST /google_results
  # POST /google_results.json
  def create
    @google_result = GoogleResult.new(google_result_params)

    respond_to do |format|
      if @google_result.save
        format.html { redirect_to @google_result, notice: 'Google result was successfully created.' }
        format.json { render :show, status: :created, location: @google_result }
      else
        format.html { render :new }
        format.json { render json: @google_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /google_results/1
  # PATCH/PUT /google_results/1.json
  def update
    respond_to do |format|
      if @google_result.update(google_result_params)
        format.html { redirect_to @google_result, notice: 'Google result was successfully updated.' }
        format.json { render :show, status: :ok, location: @google_result }
      else
        format.html { render :edit }
        format.json { render json: @google_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /google_results/1
  # DELETE /google_results/1.json
  def destroy
    @google_result.destroy
    respond_to do |format|
      format.html { redirect_to google_results_url, notice: 'Google result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_google_result
      @google_result = GoogleResult.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def google_result_params
      params.require(:google_result).permit(:keyword, :top_ads_count, :bottom_ads_count, :total_ads_count, :top_ads_urls, :bottom_ads_urls, :non_ads_urls, :links_count, :results_count, :results_search_time, :html_cache)
    end
end
