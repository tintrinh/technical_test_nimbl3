class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    super
    if current_user.present?
      access_token = Doorkeeper::AccessToken.find_by(resource_owner_id: current_user.id)

      if access_token.present? && !access_token.expired?
        token = access_token.token
      else
        token = Doorkeeper::AccessToken.create!(application_id: nil, 
          resource_owner_id: current_user.id, 
          expires_in: 2.hours
          ).token
      end
      cookies[:token] = token
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
