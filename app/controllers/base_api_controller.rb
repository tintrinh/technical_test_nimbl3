class BaseApiController < ApplicationController
  skip_before_action  :verify_authenticity_token
  before_action :doorkeeper_authorize! # Require access token for all actions
  before_filter :parse_request

  private

  def parse_request
    # @json = JSON.parse(request.body.read)
    @api_token = request.headers["Api-Token"]
  end

  protected

  def render_error_message(message)
    render json: { message: message }, status: 501
  end
end