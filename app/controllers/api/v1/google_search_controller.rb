module Api
  module V1
    class GoogleSearchController < BaseApiController
      respond_to :json

      def search
        file = params[:keywords_csv]

        # validate extention
        if File.extname(file.original_filename) != '.csv'
          return render_error_message('Please upload correct csv file.')
        end

        csv = Roo::CSV.new(file.path)

        # validate header of csv file
        if csv.row(1) != ["id","keyword"]
          return render_error_message('Please correct the header.')
        end

        # get all keywords
        keywords = csv.column(2)
        keywords.shift(1)

        # create upload history
        upload_history = UploadHistory.create(file_name: file.original_filename)

        # search all keywords
        keywords.each do |k|
          search_request(k, upload_history)
        end

        # make upload done in database
        upload_history.update(status: true)

        render json: { status: 200, message: 'Success' }
      rescue => e
        # make upload done in database
        upload_history.update(status: true)
        render_error_message(e.message)
        logger.error(e.message)
        logger.error(e.backtrace)
      end

      # get results by upload history
      def report
        upload_id = params[:id]
        results = GoogleResult.by_upload(upload_id).order(:keyword)
        results.each do |r|
          r.top_ads_urls = r.top_ads_urls.gsub(',', "\n")
          r.bottom_ads_urls = r.bottom_ads_urls.gsub(',', "\n")
          r.non_ads_urls = r.non_ads_urls.gsub(',', "\n")
          r.html_cache = r.html_cache.force_encoding('UTF-8')
        end
        render json: { status: 200, data: results }
      end

      private

      def search_request(k, upload_history)
        logger.info(k)
        keyword = k.gsub(' ', '+')
        result = `curl -A "Mozilla/5.0 (X11; Linux x86_64) \
          AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36" \
          'https://www.google.com.vn/search?q=#{keyword}'`
        html = Nokogiri::HTML(result)

        # get related ads info
        top_ads = html.css('#taw .ads-ad')
        bottom_ads = html.css('#bottomads .ads-ad')
        top_ads_urls_a = []
        bottom_ads_urls_a = []
        top_ads.each { |ads| top_ads_urls_a << ads.css('cite') }
        bottom_ads.each { |ads| bottom_ads_urls_a << ads.css('cite') }

        # get non ads urls
        non_ads_urls_a = []
        html.css('.srg .g h3 a').each { |g| non_ads_urls_a << g['href'] }

        # get total results
        logger.info(html.css('#resultStats'))
        total_results = html.css('#resultStats')[0].text.split(' ')[1]

        # get results search time
        results_search = html.css('#resultStats nobr')[0].text.strip.split(' ')[0]

        # save info to GoogleResult table
        top_ads_count = top_ads.length
        bottom_ads_count = bottom_ads.length
        total_ads_count = top_ads_count + bottom_ads_count

        upload_history.google_results.create!({
          keyword: k,
          top_ads_count: top_ads.length,
          bottom_ads_count: bottom_ads.length,
          total_ads_count: total_ads_count,
          top_ads_urls: top_ads_urls_a.join(','),
          bottom_ads_urls: bottom_ads_urls_a.join(','),
          non_ads_urls: non_ads_urls_a.join(','),
          links_count: total_ads_count + non_ads_urls_a.length,
          results_count: total_results.gsub(/[,.]/, '').to_i,
          results_search_time: results_search[1..-1].gsub(/[,]/, '.').to_f,
          html_cache: html
        })
      rescue => e
        logger.error(e.message)
        logger.error(e.backtrace)
      end
    end
  end
end
