class UploadHistory < ApplicationRecord
  has_many :google_results, dependent: :destroy
end
