class GoogleResult < ApplicationRecord
  belongs_to :upload_history

  scope :by_upload, -> (upload_id) { where(upload_history_id: upload_id) }
end
