json.extract! google_result, :id, :keyword, :top_ads_count, :bottom_ads_count, :total_ads_count, :top_ads_urls, :bottom_ads_urls, :non_ads_urls, :links_count, :results_count, :results_search_time, :html_cache, :created_at, :updated_at
json.url google_result_url(google_result, format: :json)
