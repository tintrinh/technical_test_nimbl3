Rails.application.routes.draw do
  use_doorkeeper do
    skip_controllers :authorizations, :applications,
      :authorized_applications
  end
  resources :google_results
  # devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'google_results#index'
  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'sign_up' }, controllers: {sessions: "users/sessions"}

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
  end

  scope '/api' do
    scope '/v1' do
      scope '/google_search' do
        post '/' => 'api/v1/google_search#search'
        get 'report/:id' => 'api/v1/google_search#report'
      end
    end
  end

  get 'template_csv' => 'users#template_csv'
end
