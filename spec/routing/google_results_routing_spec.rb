require "rails_helper"

RSpec.describe GoogleResultsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/google_results").to route_to("google_results#index")
    end

    it "routes to #new" do
      expect(:get => "/google_results/new").to route_to("google_results#new")
    end

    it "routes to #show" do
      expect(:get => "/google_results/1").to route_to("google_results#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/google_results/1/edit").to route_to("google_results#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/google_results").to route_to("google_results#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/google_results/1").to route_to("google_results#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/google_results/1").to route_to("google_results#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/google_results/1").to route_to("google_results#destroy", :id => "1")
    end

  end
end
