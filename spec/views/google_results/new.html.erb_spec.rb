require 'rails_helper'

RSpec.describe "google_results/new", type: :view do
  before(:each) do
    assign(:google_result, GoogleResult.new(
      :keyword => "MyString",
      :top_ads_count => "",
      :bottom_ads_count => "",
      :total_ads_count => "",
      :top_ads_urls => "MyString",
      :bottom_ads_urls => "MyString",
      :non_ads_urls => "MyString",
      :links_count => "",
      :results_count => 1,
      :results_search_time => "",
      :html_cache => "MyText"
    ))
  end

  it "renders new google_result form" do
    render

    assert_select "form[action=?][method=?]", google_results_path, "post" do

      assert_select "input#google_result_keyword[name=?]", "google_result[keyword]"

      assert_select "input#google_result_top_ads_count[name=?]", "google_result[top_ads_count]"

      assert_select "input#google_result_bottom_ads_count[name=?]", "google_result[bottom_ads_count]"

      assert_select "input#google_result_total_ads_count[name=?]", "google_result[total_ads_count]"

      assert_select "input#google_result_top_ads_urls[name=?]", "google_result[top_ads_urls]"

      assert_select "input#google_result_bottom_ads_urls[name=?]", "google_result[bottom_ads_urls]"

      assert_select "input#google_result_non_ads_urls[name=?]", "google_result[non_ads_urls]"

      assert_select "input#google_result_links_count[name=?]", "google_result[links_count]"

      assert_select "input#google_result_results_count[name=?]", "google_result[results_count]"

      assert_select "input#google_result_results_search_time[name=?]", "google_result[results_search_time]"

      assert_select "textarea#google_result_html_cache[name=?]", "google_result[html_cache]"
    end
  end
end
