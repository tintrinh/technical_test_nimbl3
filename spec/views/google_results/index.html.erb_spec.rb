require 'rails_helper'

RSpec.describe "google_results/index", type: :view do
  before(:each) do
    assign(:google_results, [
      GoogleResult.create!(
        :keyword => "Keyword",
        :top_ads_count => "",
        :bottom_ads_count => "",
        :total_ads_count => "",
        :top_ads_urls => "Top Ads Urls",
        :bottom_ads_urls => "Bottom Ads Urls",
        :non_ads_urls => "Non Ads Urls",
        :links_count => "",
        :results_count => 2,
        :results_search_time => "",
        :html_cache => "MyText"
      ),
      GoogleResult.create!(
        :keyword => "Keyword",
        :top_ads_count => "",
        :bottom_ads_count => "",
        :total_ads_count => "",
        :top_ads_urls => "Top Ads Urls",
        :bottom_ads_urls => "Bottom Ads Urls",
        :non_ads_urls => "Non Ads Urls",
        :links_count => "",
        :results_count => 2,
        :results_search_time => "",
        :html_cache => "MyText"
      )
    ])
  end

  it "renders a list of google_results" do
    render
    assert_select "tr>td", :text => "Keyword".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Top Ads Urls".to_s, :count => 2
    assert_select "tr>td", :text => "Bottom Ads Urls".to_s, :count => 2
    assert_select "tr>td", :text => "Non Ads Urls".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
