require 'rails_helper'

RSpec.describe "google_results/show", type: :view do
  before(:each) do
    @google_result = assign(:google_result, GoogleResult.create!(
      :keyword => "Keyword",
      :top_ads_count => "",
      :bottom_ads_count => "",
      :total_ads_count => "",
      :top_ads_urls => "Top Ads Urls",
      :bottom_ads_urls => "Bottom Ads Urls",
      :non_ads_urls => "Non Ads Urls",
      :links_count => "",
      :results_count => 2,
      :results_search_time => "",
      :html_cache => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Keyword/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Top Ads Urls/)
    expect(rendered).to match(/Bottom Ads Urls/)
    expect(rendered).to match(/Non Ads Urls/)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
  end
end
