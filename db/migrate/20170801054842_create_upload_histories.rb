class CreateUploadHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :upload_histories do |t|
      t.string :file_name
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
