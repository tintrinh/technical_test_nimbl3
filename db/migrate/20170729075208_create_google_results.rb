class CreateGoogleResults < ActiveRecord::Migration[5.0]
  def change
    create_table :google_results do |t|
      t.string :keyword
      t.integer :top_ads_count
      t.integer :bottom_ads_count
      t.integer :total_ads_count
      t.string :top_ads_urls
      t.string :bottom_ads_urls
      t.string :non_ads_urls
      t.integer :links_count
      t.string :results_count
      t.float :results_search_time
      t.binary :html_cache
      t.integer :upload_history_id

      t.timestamps
    end

    add_index :google_results, :keyword
  end
end
