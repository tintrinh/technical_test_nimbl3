# README

Technical test at Nimbl3
Screenshot: https://gyazo.com/6673cd66b9fa77aae57bbac3aadf3448

## Acknowledgments 

1. Authenticate with OAuth 2
2. Upload and search keywords at google.com
3. Store ěnformation at first page of search result
4. Report stored information

## Enhancement

1. Add relationship/permistion for search and users (current all users see the same result)
2. Implement query function
3. Implement unit tests

## How to use

1. Go to website https://fast-plains-40484.herokuapp.com/
2. Sign up an account
3. Then log out and Log in again to have an api token
4. Download template csv of key words
5. Update you wanted key words
6. Upload
7. Choose an upload version to see the result

Thank you
